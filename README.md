## Experimental in Drupal core since version 9.3.0!

This module is no longer maintained; it's experimental in Drupal core since version 9.3.0.

See https://www.drupal.org/project/ckeditor5
